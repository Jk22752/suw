<?php
//Wyświetlanie błędów na potrzeby developerki
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//Definiowanie stałych i ustawienia regionalne
define('includeSecurity', TRUE);
setlocale(LC_ALL, ['pl_PL.UTF-8', 'pl_PL', 'pl', 'Polish_Poland.65001', 'polish_poland']);
date_default_timezone_set("Europe/Warsaw");

//Inicjowanie sesji i bufora wyjścia
ob_start();
session_start();

//Definiowanie zmiennych
global $page;
global $userType;
$page = (isset($_GET['page']) && !empty($_GET['page'])) ? $_GET['page'] : 'main';
include 'autoload.php'; //Autoładowanie klas
Login::loginCheck();
$userType = User::getUserType(Login::$login);
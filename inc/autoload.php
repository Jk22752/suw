<?php
spl_autoload_register(function ($class_name) {
    if (!strpos($class_name, '\\')) {
        set_include_path(realpath(getcwd()));
        include 'classes/' . $class_name . '.php';
    }
});
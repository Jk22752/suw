<?php
class Login
{
    public static $logged = FALSE;
    public static $login = NULL;

    /** Dodaje użytkownika do tabeli zalogowanych
     * @param $user
     * @return bool
     */
    static public function addLogged($user)
    {
        $errors = Warning::count();
        session_regenerate_id(TRUE);
        $user->lastActivity = date('Y-m-d H:i:s');
        if (session_status() == PHP_SESSION_NONE) session_start();
        $user->sessionId = session_id();
        Db::executeQuery('INSERT INTO `logged`(`userId`, `sessionId`, `lastActivity`) VALUES(:userId, :sessionId, :lastActivity)', $user);
        if ($errors === Warning::count()) {
            Warning::set('Zalogowano pomyślnie');
            self::$logged = TRUE;
            self::$login = $user->transcriptId;
            self::setSessionVars();
            return TRUE;
        }
        return FALSE;
    }

    private static function setSessionVars()
    {
        $_SESSION['login'] = self::$login;
        $_SESSION['logged'] = self::$logged;
    }

    static public function loginCheck()
    {
        if (isset($_SESSION['login']) && isset($_SESSION['logged'])
            && !empty($_SESSION['login']) && !empty($_SESSION['logged'])) {
            self::$login = $_SESSION['login'];
            self::$logged = $_SESSION['logged'];
            $user = User::getUser(self::$login);
            $user = Db::executeQuery('SELECT * FROM `logged` WHERE `userId`=:userId', $user);
            if ($user) {
                $user = $user->fetchObject();
                if ($user->sessionId === session_id()) {
                    $now = strtotime('now');
                    $lastActivity = strtotime($user->lastActivity);
                    $diff = floor(($now - $lastActivity) / 60);
                    if ($diff < 30) {
                        self::setLastActivity();
                        return TRUE;
                    }
                }
            }
            Login::logOut('Wylogowano z powodu braku aktywności');
        }
        return FALSE;
    }

    static private function setLastActivity()
    {
        if (self::$logged === TRUE && !empty(self::$login)) {
            if (!isset($user) || empty($user)) {
                $user = User::getUser(self::$login);
            }
            $lastActivity = date('Y-m-d H:i:s');
            $user->lastActivity = $lastActivity;
            $user->sessionId = session_id();
            Db::executeQuery('UPDATE `users` SET `lastActivity`=:lastActivity WHERE `userId`=:userId', $user);
            return (Db::executeQuery('UPDATE `logged` SET `lastActivity`=:lastActivity WHERE `userId`=:userId', $user)) ? TRUE : FALSE;
        }
        return FALSE;
    }

    static public function logOut($msg)
    {
        if (!isset($user) || empty($user)) {
            $user = User::getUser(self::$login);
        }
        Db::executeQuery('DELETE FROM `logged` WHERE userId=:userId', $user);
        self::$logged = FALSE;
        self::$login = NULL;
        self::setSessionVars();
        session_unset();
        session_destroy();
        session_write_close();
        setcookie(session_name(), '', 0, '/');
        session_start();
        session_regenerate_id(TRUE);
        Warning::set($msg);
        http_response_code(205);
        header('Location: index.php');
        exit();
    }

    static public function getCurrentLogged()
    {
        $result = Db::executeQuery('SELECT COUNT(*) FROM `logged`;');
        return ($result) ? $result->fetch(Db::FETCH_NUM)[0] : '0';
    }
}
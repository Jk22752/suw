<?php
    include_once 'modules.php';

    class DatabaseMgmt {
        /**
         * Usage:
         *      $dbmgnt = DatabaseMgmt();
         *      $result = $dbmgmt->execute_select_query($query);
         *      $dbmgnt->disconnect();
         *      ...
         *      $dbmgnt->connect();
         *      $result = $dbmgmt->execute_select_query($query);
         *      $dbmgnt->disconnect();
         * 
         */
        private $db_password;
        private $db_login;
        private $db_name;
        private $db_servername;

        private $connection;

        public function __construct() {
            global $config_data;
            $this->db_servername = Config::get('dbAddress');
            $this->db_name = Config::get('dbName');
            $this->db_login = Config::get('dbLogin');
            $this->db_password = Config::get('dbPass');
            $this->connection = Null;
            $this->connect();
        }

        public function connect() {
            if ($this->connection === Null) {
                $this->connection = new mysqli(
                    $this->db_servername,
                    $this->db_login,
                    $this->db_password,
                    $this->db_name
                );
                if ($this->connection->connect_errno) {
                    Warning::set(
                        'Database connection error: '.$this->connection->connect_errno
                    );
                    die(Warning::print());
                }
            }
        }

        public function disconnect() {
            if ($this->connection) {
                $this->connection->close();
                $this->connection = Null;
            }
        }

        public function execute_select_query($query, $exit_if_nf=TRUE) {
            $result = $this->connection->query($query);

            if (!$result or $result->num_rows < 1) {
                Warning::set(
                    'No rows found.'
                );
                if ($exit_if_nf) {
                    die(Warning::print());
                }
            }

            return $result;
        }

        public function execute_insert_query($query) {
            $result = $this->connection->query($query);
            if(!$result) {
                return false;
            }

            return true;
        }
    }
?>
<?php
class Table
{
    private static $data;
    private static $headers;
    private static $printHeaders;
    private static $page;
    private static $table;
    private static $maxPerPage = 15;
    private static $offset = 0;

    /**
     * @param array $data - tabela asocjacyjna (np użytkownicy, pobrania)
     * @param array $headers - tabela asocjacyjna z nagłówkami kolumn, które chcemy wyświetlić, wraz z  tłumaczeniem
     * np:     $naglowki = array(
     * "userId" => "Id użytkownika",
     * "downloadTime" => "Czas pobrania",
     * "ip" => "Numer IP",
     * "fileId" => "Czas logowania"
     * );
     * Jak nie chcemy wyświetlić kolumny userId to po prostu nie wpisujemy tej nazwy do tabeli $headers
     */
    public static function create(array $data, array $headers = null)
    {
        self::$data = $data;
        self::$page = isset($_GET['p']) && !empty($_GET['p']) ? $_GET['p'] : 1;
        if (!empty($headers)) {
            self::$printHeaders = array_values($headers);
            self::$headers = array_keys($headers);
        } else {
            self::$headers = array_keys(self::$data[0]);
            self::$printHeaders = self::$headers;
        }
        self::trimData();
        self::$table .= self::createHeaders();
        self::$table .= implode('', (!empty(self::$page)) ? (array_slice(self::createBody(), (self::$page - 1) * 15, 15, true)) : self::createBody());
        self::$table .= '</tbody></table>';
        echo self::$table;
        echo self::createPages();
    }

    private static function trimData()
    {
        $diff = array_diff(array_keys(self::$data[0]), self::$headers);
        foreach (self::$data as $num => $row) {
            foreach ($row as $header => $cell) {
                if (in_array($header, $diff)) {
                    unset(self::$data[$num][$header]);
                }
            }
        }
    }
//class="thead-light"
    private static function createHeaders()
    {
        $headers = '<table class="table table-sm table-striped table-hover"><thead ><tr class="bg-primary text-white">';
        $headers .= '<th scope="col">#</th>';
        foreach (self::$printHeaders as $header) $headers .= '<th scope="col">' . $header . '</th>';
        $headers .= '</tr></thead><tbody>';
        return $headers;
    }

    private static function createBody()
    {
        $nr = 1;
        $body=array();
        global $page;
        if (in_array($page,Load::$accesControl['Admin'])) {
            reset(self::$data);
            $id=key(self::$data[0]);
            $mod=isset($_GET['mod'])?$_GET['mod']:'';
            foreach (self::$data as $rNum => $row) {
                $body[] .= "<tr>";
                $body[$rNum] .= '<th scope="row"><a href="?page=adminShow&mod='.$mod.'&id='.self::$data[$rNum][$id].'">' . $nr++ . '</a></th>';
                foreach ($row as $cell) {
                    $body[$rNum] .= '<td><a href="?page=adminShow&mod='.$mod.'&id='.self::$data[$rNum][$id].'">'.$cell.'</a></td>';
                }
                $body[$rNum] .= '</tr>';
            }
        } else {
            foreach (self::$data as $rNum => $row) {
                $body[] .= "<tr>";
                $body[$rNum] .= '<th scope="row">' . $nr++ . '</th>';
                foreach ($row as $cell) {
                    $body[$rNum] .= '<td>'.$cell.'</td>';
                }
                $body[$rNum] .= '</tr>';
            }
        }
        return $body;
    }

    private static function createPages()
    {
        $currentPage = self::$page;
        $pageCount = (int)ceil(count(self::$data) / 15);
        if ($pageCount > 1) {
            $pages = '<nav aria-label="Pagination"><ul class="pagination justify-content-center"><li class="page-item';
            $pages .= (empty($currentPage) || $currentPage == 1) ? ' disabled' : '';
            $pages .= '"><a class="page-link" href="' . self::createUrl() . ($currentPage - 1) . '" tabindex="-1">Previous</a></li>';
            for ($i = 1; $i <= $pageCount; $i++) {
                $pages .= '<li class="page-item' . (($currentPage == $i) ? ' active' : '') . '"><a class="page-link" href="' . self::createUrl() . $i . '">' . $i . '</a></li>';
            }
            $pages .= '<li class="page-item';
            $pages .= ($currentPage == $pageCount) ? ' disabled' : '';
            $pages .= '"><a class="page-link" href="' . self::createUrl() . ($currentPage + 1) . '">Next</a></li></ul></nav>';
            return $pages;
        }
    }

    private static function createUrl()
    {
        if (isset($_GET['p']) && !empty($_GET['p'])) {
            return rtrim($_SERVER['REQUEST_URI'], "0..9");
        } else
            return $_SERVER['REQUEST_URI'] . '&p=';
    }
}
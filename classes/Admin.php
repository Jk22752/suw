<?php

class Admin
{
    public static $translations =
        array(
            'users' => 'Zarządzanie użytkownikami',
            'files' => 'Zarządzanie plikami',
            'courses' => 'Zarządzanie kursami',
            'downloads' => 'Wyświetlanie logów',
            'priviledges' => 'Wyświetlanie uprawnień',
            'logged' => 'Wyświetlanie uprawnień'
        );
    public static $queries = array(
        'users' => 'SELECT * FROM users',
        'files' => 'SELECT * FROM files',
        'courses' => 'SELECT * FROM courses',
        'downloads' => 'SELECT * FROM downloads',
        'priviledges' => 'SELECT * FROM priviledges',
        'logged' => 'SELECT * FROM logged'
    );
    public static $tables = array(
        'users' => array(
            'userId' => 'ID użytkownika',
            'password' => 'Hasło',
            'transcriptId' => 'Numer indeksu/użytkownika',
            'lastActivity' => 'Ostatnio aktywny',
        ),
        'files' => array(
            'fileId' => 'ID pliku',
            'courseId' => 'ID kursu',
            'userId' => 'ID użytkownika',
            'title' => 'Tytuł',
            'description' => 'Opis',
            'size' => 'Rozmiar',
            'uploadTime' => 'Data udostępnienia',
            'type' => 'Typ',
            'fileName' => 'Nazwa pliku',
        ),
        'courses' => array(
            'courseId' => 'ID kursu',
            'name' => 'Nazwa kursu',
            'userId' => 'ID prowadzącego',
        ),
        'downloads' => array(
            'downloadId' => 'ID pobrania',
            'downloadTime' => 'Data pobrania',
            'fileId' => 'ID pliku',
            'userId' => 'ID użytkownika',
            'ip' => 'IP',
        ),
        'priviledges' => array(
            'priviledgeId' => 'ID uprawnienia',
            'userId' => 'ID użytkownika',
            'fileId' => 'ID pliku',
            'clearanceLevel' => 'Poziom uprawnień',
        ),
        'logged' => array(
            'userId' => 'ID użytkownika',
            'sessionId' => 'ID sesji',
            'lastActivity' => 'Ostatnio aktywny',
        ),
    );
    public static $varTypes = [
        'numeric' => array(
        'userId', 'courseId', 'fileId', 'size', 'downloadId', 'priviledgeId', 'clearanceLevel'
    ), 'text' => array(
        'title', 'description', 'type', 'name'
    ), 'date' => array(
        'lastActivity', 'uploadTime', 'downloadTime'
    ),
        'ip' => array('ip'),
        'fileName' => array('fileName')
    ];

    public static function validateData($data)
    {
        foreach ($data as $key => $value) {
            foreach (self::$varTypes as $type => $fieldName) {
                $typ = $type;
                if (in_array($key, $fieldName)) {
                    $nr = key($fieldName);
                    if ($typ === 'numeric' && !Validate::number($value)
                        || $typ === 'date' && !Validate::date($value)
                        || $typ === 'ip' && !Validate::ip($value)
                        || $typ === 'fileName' && !Validate::fileName($value)) {
                        Warning::set('Pole ' . $fieldName[$nr] . ' zawiera nieprawidłową wartość.');
                        break;
                    }
                }
            }
            ($key === 'password' && Validate::password($value)) ? $data[$key] = password_hash($value, PASSWORD_BCRYPT, array("cost" => 12)) : $data[$key] = $value;
        }
        return $data;
    }
}
<?php
# Klasa do przechowywania wartości konfiguracji - połączenie z bazą, domyślne wartości wspólne dla wszystkich modułów itd.
class Config
{
    private static $data;

    public static function get($key)
    {
        if (!isset(self::$data)||empty(self::$data)) {
            self::$data = include './inc/config.php';
        }
        return self::$data[$key];
    }
}
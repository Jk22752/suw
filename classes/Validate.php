<?php

/** Klasa do sprawdzania pól tekstowych oraz wszystkich innych rzeczy, które wprowadza użytkownik
 * */
class Validate
{
    static public function text($data)
    {
        $data = self::cleanInput($data);
        if (ctype_alpha($data)) {
            return TRUE;
        } else {
            Warning::set("Wpisana wartość \"$data\" zawiera znaki inne niż litery");
            return FALSE;
        }
    }

    static public function cleanInput($data)
    {
        return htmlentities(stripslashes(trim($data)), ENT_QUOTES, 'UTF-8');
    }

    static public function date($data, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $data);
        if ($d && $d->format($format) == $data) {
            return true;
        } else {
            Warning::set('Wpisana data jest niepoprawna');
            return false;
        }
    }

    static public function transcriptId($transcriptId)
    {
        $transcriptId = (int)self::cleanInput($transcriptId);
        if (is_int($transcriptId) && ($transcriptId > 0) && ($transcriptId < 100000)) {
            return TRUE;
        } else {
            Warning::set("Wpisany numer indeksu/użytkownika \"$transcriptId\" jest niepoprawny");
            return FALSE;
        }
    }

    static public function number($data)
    {
        $data = self::cleanInput($data);
        if (is_numeric($data)) {
            return TRUE;
        } else {
            Warning::set('Wpisano nieprawidłową wartość numeryczną');
            return FALSE;
        }
    }
    public static function ip($data) {
        if (!preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/', $data)) {
            Warning::set("Wpisano nieprawidłowy adres IP");
            return FALSE;
        } else
            return true;

    }
    public static function fileName($data) {
        if (!preg_match('/^[a-zA-Z0-9ąśżźćńłęóĄŚŻŹĆŃŁĘÓ ]{1,25}\.\w{2,5}$/', $data)) {
            Warning::set('Wpisano nieprawidłową nazwę pliku');
            return FALSE;
        }
        return TRUE;
    }

    public static function password($password)
    {
        $warnings = Warning::count();
        if (strlen($password) < 8) {
            Warning::set("Hasło musi posiadać co najmniej 8 znaków");
        }
        if (!preg_match('/[a-z]+|[ąśżźćńłęó]+/', $password)) {
            Warning::set("Hasło musi posiadać co najmniej 1 małą literę.");
        }
        if (!preg_match('/[A-Z]+|[ĄŚŻŹĆŃŁĘÓ]+/', $password)) {
            Warning::set("Hasło musi posiadać co najmniej 1 wielką literę.");
        }
        if (!preg_match('/\d+/', $password)) {
            Warning::set("Hasło musi posiadać co najmniej 1 cyfrę.");
        }
        if (Warning::count() == $warnings) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
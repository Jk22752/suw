<?php
# Klasa (serwis?) do zarządzania plikami z wykładami
class fileService
{
    private $storageCatalog;

    public function __construct($_storageCatalog)
    {
        $this->storageCatalog = $_storageCatalog;
    }

    // return type: string
    public function getFilePath($fileId)
    {
        if (!isset($storageCatalog) || empty($storageCatalog)) {
            $storageCatalog = 'storage';
        }


        $fileNameQuery = "SELECT fileName FROM files WHERE fileId = $fileId";
        $res = Db::executeQuery($fileNameQuery);

        if ($res) {
            $fileName = $res->fetchColumn();
            $path = $storageCatalog . "/" . $fileName;

            $userIdQuery = "SELECT userId FROM users WHERE transcriptId = " . Login::$login;
            $res2 = Db::executeQuery($userIdQuery);

            if ($res2) {
                $userId = $res2->fetchColumn();

                $date = date('Y-m-d H:i:s');
                $ip = $_SERVER['REMOTE_ADDR'];
                $updateDownloadsHistoryQuery = "INSERT INTO downloads (`downloadTime`, `fileId`, `userId`, `ip`) VALUES ('$date', $fileId, $userId, '$ip')";
                $res3 = Db::executeQuery($updateDownloadsHistoryQuery);

                if ($res3) {
                    return $path;
                } else {
                    return "";
                }
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    public function getFileAbsPathById($fileId)
    {
        if (!isset($storageCatalog) || empty($storageCatalog)) {
            $storageCatalog = 'storage';
        }

        $query = "SELECT fileName FROM files WHERE fileId='$fileId'";
        $result = Db::executeQuery($query);

        $file_name = NULL;
        foreach ($result->fetchAll() as $key => $res) {
            $file_name = $res['fileName'];
        }
        if ($file_name === NULL) {
            throw new Exception("File name not found for file ID: " . $fileId);
        }
        $storage_dir = Config::get('defaultContentStorage');

        $abs_path = Config::get('projectDirAbsPath') . DIRECTORY_SEPARATOR . $storage_dir . DIRECTORY_SEPARATOR . $file_name;
        return realpath($abs_path);
    }
}
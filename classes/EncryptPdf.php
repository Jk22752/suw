<?php
    class EncryptPdf {
        private $pdf;
        private $storage_path;

        public $pdf_password;
        public $pdf_file_id;
        public function __construct($pdf_file_id, $pdf_password) {
            $external_packages_path = realpath(Config::get('projectDirAbsPath') . "/classes");
            require_once(realpath($external_packages_path.'/setasign/fpdi-protection/src/autoload.php'));
            $this->pdf_file_id = $pdf_file_id;
            $this->pdf_password = $pdf_password;
            $this->pdf = new \setasign\FpdiProtection\FpdiProtection();
            $this->storage_path = realpath(Config::get('projectDirAbsPath').'/storage');
        }

        private function get_file_abspath() {
            $file_service = new fileService($this->storage_path);
            return $file_service->getFileAbsPathById($this->pdf_file_id);
        }

        public function perform_encryption() {
            $file = $this->get_file_abspath();
            try {
                $pagecount = $this->pdf->setSourceFile($file);
            } catch(Exception $crex) {
                Warning::set(
                    'File is corrupted or already encrypted. Error: '//.$crex
                );
                die(Warning::print());
            }
            for ($i = 1; $i <= $pagecount; $i++) {
                $page = $this->pdf->importPage($i);
                $size = $this->pdf->getTemplateSize($page);
                $page_orient = $size['orientation'];
                $this->pdf->addPage($page_orient);
                $this->pdf->useTemplate($page);
            }
            $this->pdf->SetProtection(array(), $this->pdf_password);
            $this->pdf->Output($file, 'F');
        }
    }

?>
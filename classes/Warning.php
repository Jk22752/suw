<?php
/*************************************************************************************
 * set( [string] ) - dodaje do tablicy błędów komunikat;                      *
 * get() - zwraca całą tablicę błędów, jeżeli jest pusta zwraca false;        *
 * print() - zwraca string z całej tablicy błędów, komunikaty oddzielone <br>,*
 * jeżeli jest pusta zwraca false;                                                   *
 * count() - zwraca liczbę błędów w tablicy, tym możecie sprawdzić czy nie    *
 * było błędów w zapytaniach SQL i czy można kontynuować wykonywanie skryptu...      *
 * jak robicie obsługę błędów to dajcie tylko set, a automatycznie pojawi się *
 * na górze okienka z treścią;                                                       *
 ************************************************************************************/

class Warning
{
    static public function set($warning)
    {
        if (!isset($_SESSION['error'])) {
            $_SESSION['error'] = array();
        }
        array_push($_SESSION['error'], $warning);
    }

    static public function print()
    {
        if (self::get()) {
            $warnings = (string)'';
            foreach ($_SESSION['error'] as $warning) {
                $warnings .= $warning . '<br>';
            }
            return $warnings;
        } else {
            return FALSE;
        }
    }
    static public function display() {
        if (Warning::print() !== FALSE) {
            $html = ob_get_clean();
            $search = array(' hidden', 'ERROR_TAG');
            $replace = array('', Warning::print());
            Warning::clean();
            $html = str_replace($search, $replace, $html);
            print($html);
        } else {
            ob_end_flush();
        }
    }

    static public function get()
    {
        if (isset($_SESSION['error']) && !empty($_SESSION['error'])) {
            return $_SESSION['error'];
        } else {
            return FALSE;
        }
    }

    static public function count()
    {
        if (isset($_SESSION['error']) && !empty($_SESSION['error'])) {
            return count($_SESSION['error']);
        } else {
            return 0;
        }
    }

    static public function clean()
    {
        if (isset($_SESSION['error']) && !empty($_SESSION['error'])) {
            unset($_SESSION['error']);
        }
    }
}
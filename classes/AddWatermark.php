<?php

//use \setasign\Fpdi\Fpdi;

$external_packages_path = realpath(Config::get('projectDirAbsPath') . "/vendor");
include_once(realpath($external_packages_path . '/setasign/fpdf/fpdf.php'));
include_once(realpath($external_packages_path . '/setasign/fpdi/src/autoload.php'));
require_once(realpath($external_packages_path . '/setasign/fpdi-protection/src/autoload.php'));
class AddWatermark
{
    /**
     * Required libraries:
     * - FPDF 1.8.1
     * - FPDI 2.1.1
     *
     * Composer installation:
     * - https://getcomposer.org/download/
     * (Command-line installation) -> enter server shell and do the installation
     * installation have to be performed in suw/ directory
     *
     * Composer usage:
     * cd suw/
     * php composer.phar install
     *
     * Class usage:
     * $waterm = new AddWatermark($pdf_filename);
     * $waterm->add_watermark();
     */
    private $storage_path;
    private $waterm_text = 'MARKED: ZUT SUW test';
    private $pdf;
    public $pdf_file_id;
    public $pdf_password;
    public function __construct($pdf_file_id)
    {
        $this->pdf_file_id = $pdf_file_id;
        $this->storage_path = realpath(Config::get('projectDirAbsPath') . '/storage');
        $this->pdf = new \setasign\FpdiProtection\FpdiProtection();
    }

    public function setText($text)
    {
        $this->waterm_text = $text;
    }

    public function add_watermark()
    {
        $file = $this->get_file_abspath();
        try {
            $page_count = $this->pdf->setSourceFile($file);
        } catch (Exception $pdfp_e) {
            Warning::set(
                'File is corrupted. Error: ' . $pdfp_e
            );
            return false;
        }
        for ($i = 1; $i <= $page_count; $i++) {
            $page_templ = $this->pdf->importPage($i);
            $size = $this->pdf->getTemplateSize($page_templ);
            $page_orient = $size['orientation'];
            $page_width = (int)$size['width'];
            $page_height = (int)$size['height'];
            $this->pdf->addPage($page_orient);
            $this->pdf->SetFont('Times', 'I', 50);
            $this->pdf->SetTextColor(206, 204, 204);
            $text_x_pos = ceil((40 / 100) * $page_width); // Watermark position: % of page width, % of page height
            $text_y_pos = ceil((20 / 100) * $page_height);
            $this->pdf->SetXY($text_x_pos, $text_y_pos);
            $this->pdf->Write(0, $this->waterm_text[0]);
            $text_x_pos = ceil((20 / 100) * $page_width);
            $text_y_pos = ceil((50 / 100) * $page_height);
            $this->pdf->SetXY($text_x_pos, $text_y_pos);
            $this->pdf->Write(0, $this->waterm_text[1]);
            $text_x_pos = ceil((20 / 100) * $page_width);
            $text_y_pos = ceil((70 / 100) * $page_height);
            $this->pdf->SetXY($text_x_pos, $text_y_pos);
            $this->pdf->Write(0, $this->waterm_text[2]);
            $this->pdf->useTemplate($page_templ);
        }
        $this->pdf->SetProtection(array(), $this->pdf_password);
        return;
    }

    public function outputFile($file)
    {
        $this->pdf->Output('I', $file);
    }

    public function downloadFile($file)
    {
        $this->pdf->Output('D', $file);
    }

    private function get_file_abspath()
    {
        $file_service = new fileService($this->storage_path);
        return $file_service->getFileAbsPathById($this->pdf_file_id);
    }
}
?>


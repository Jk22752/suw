<?php
/*********************************
 * Każda podstrona z wrażliwym kodem  powinna zawierać te dwie linijki:
 * include_once 'modules.php';
 * Load::security();
 * Dzieki nim jak uzytkownik wejdzie na taką stronę zostanie mu wyświetlony
 * odpowiedni komunikat (można zrobić ładne strony błędów) i zostanie przekierowany na strone główną
 * funkcją
 *********************************/
class Load
{
    static public $name = 'main';
    static public $accesControl = array(
        'Niezalogowany' => array ('main', 'login', 'register', 'stats'), //niezalogowany
        'Student' => array ('fileSearch', 'fileDownload', 'userPanel'), //student
        'Wykładowca' => array ('fileUpload', 'userManagement', 'permManagement'), //wykładowca
        'Admin' => array('admin', 'adminSearch', 'adminShow', 'adminDelete') //admin
    );

    static public function security()
    {
        if (!defined('includeSecurity')) {
            self::sendHeader(401);
        }
    }

    static private function sendHeader($code)
    {
        ob_clean();
        if ($code == 404) {
            http_response_code(404);
            header("Refresh:5; url=" . self::getUrl());
            echo '<h1>404 Not Found</h1><h3>Wystąpił błąd i strona którą chcesz odwiedzić jest niedostępna.<br />Za chwilę zostaniesz przekierowany na <a href="' . self::getUrl() . '">stronę główną</a></h3>';
        } else if ($code == 401) {
            http_response_code(401);
            header("Refresh:5; url=" . self::getUrl() . "index.php?page=login");
            echo '<h1>401 Unauthorized</h1><h3>Wystąpił błąd i nie masz uprawnień aby wejść na stronę, którą chcesz odwiedzić.<br /> Za chwilę zostaniesz przekierowany na <a href="' . self::getUrl() . 'index.php?page=login">stronę logowania</a></h3>';
        } else {
            http_response_code(500);
            header("Refresh:5; url=" . self::getUrl());
            echo '<h1>500 Internal Server Error</h1><h3>Wystąpił wewnętrzny błąd serwera. Spróbuj ponownie za chwilę.<br />Za chwilę zostaniesz przekierowany na <a href="' . self::getUrl() . '">stronę główną</a></h3>';
        }
        exit();
    }

    static public function getUrl()
    {
        return 'http://' . $_SERVER['HTTP_HOST'] . '/';
    }

    static public function getPage($name)
    {
        global $userType;
        $credentials=array();
        foreach (self::$accesControl as $key => $val)
        {
            $credentials=array_merge($credentials,$val);
            if ($userType==$key) break;
        }
        if (in_array($name, $credentials)) {
            self::incl($name);
        } else {
            foreach (self::$accesControl as $val) {
                if (in_array($name, $val)) {
                    self::sendHeader(401);
                    return true;
                }
            }
            self::sendHeader(404);
            return true;
        }
    }

    static private function incl($name)
    {
        $rootDir = Config::get('projectDirAbsPath');
        if (file_exists($rootDir . "/modules/$name.php")) {
            include $rootDir . "/modules/$name.php";
        } else {
            self::sendHeader(404);
        }
    }
}
<?php
/** Klasa do obsługi bazy danych
 * Wystarczy wywołać funkcję executeQuery a klasa zrobi całą resztę
 * Tworzymy zapytanie SQL ($query), podajemy obiekt który ma zostać wstawiony do bazy danych ($download)
 *      $download=(object)['downloadTime'=> '2018-11-16 06:06:06',
 *                     'fileId'      =>  2,
 *                     'userId'      =>  2
 *      ];
 *      $query = 'INSERT INTO downloads (downloadTime, fileId, userId) VALUES (:downloadTime,:fileId,:userId)';
 *      $result=Db::executeQuery($query,$download);
 * Potem w zależności od typu zapytania możemy pobrać wynik np.:
 *      $rows=$result->fetchAll(Db::FETCH_ASSOC); - zwraca tablicę asocjacyjną z wynikiem
 *      $object=$result->fetchObject(); - zwraca wynik w formie obiektu
 * przy INSERT albo UPDATE $result zwraca true albo false,
 * Nie róbcie nigdzie w ifach die() albo exit() bo wywalacie cały skrypt, a wystarczy w metodzie/funkcji dać
 * return false; i wyświetlić ładny komunikat użytkownikowi i na przykład przekierować go gdzieś
 * Metoda executeQuery automatycznie pobierze wymagane pola do zapytania z obiektu.
 */
class Db extends PDO
{
    private static $db;
    private static $pdoOptions = array(
        Db::ATTR_ERRMODE => Db::ERRMODE_EXCEPTION,
        Db::ATTR_EMULATE_PREPARES => FALSE,
//        Db::ATTR_DEFAULT_FETCH_MODE => Db::FETCH_ASSOC
    );

    public static function executeQuery($query, $object = NULL)
    {
        if (self::connect()) {
            try {
                $stmt = self::$db->prepare($query);
                if (!empty($object) && preg_match_all('/:[a-zA-Z]+/', $query, $keys)) {
                    $keys = str_replace(':', '', $keys[0]);
                    foreach ($keys as $val) {
                        if (property_exists($object, $val)) {
                            (is_int($object->$val)) ? $stmt->bindValue(':' . $val, $object->$val, Db::PARAM_INT) : $stmt->bindValue(':' . $val, $object->$val);
                        }
                    }
                }
                $result = $stmt->execute();
                return ($result) ? $stmt : $result;
            } catch (PDOException $exception) {
                Warning::set('Błąd zapytania do bazy danych' . $exception->getMessage());
                return FALSE;
            }
        }
        return FALSE;
    }

    private static function connect()
    {
        if (!isset(self::$db) || empty(self::$db)) {
            try {
                self::$db = new Db(
                    "mysql:host=" . Config::get('dbAddress') . ";dbname=" . Config::get('dbName') . ';charset=utf8', //DSN
                    Config::get('dbLogin'), //Username
                    Config::get('dbPass'), //Password
                    self::$pdoOptions //Options
                );
                return self::$db;
            } catch (PDOException $exception) {
                Warning::set('Błąd połączenia z bazą danych<br>'); # . $exception->getMessage());
                return FALSE;
            }
        }
        return TRUE;
    }
}
<?php

/***
 * Class User
 * Klasa do obsługi użytkowników
 *
 */
class User
{
    public $userId;
    public $transcriptId;
    public $password;
    public $sessionId;
    public $lastActivity;

    public function __construct($transcriptId = NULL, $password = NULL, $sessionId = NULL, $lastActivity = NULL)
    {
        $this->transcriptId = $transcriptId;
        $this->password = $password;
        $this->sessionId = $sessionId;
        $this->lastActivity = $lastActivity;
    }

    /** Dodaje użytkownika o podanym numerze indeksu i haśle w postaci zahaszowanej
     * @param int $transcriptId
     * @param $password
     * @return bool|User
     */
    static public function setUser(int $transcriptId, $password)
    {
        $errors = Warning::count();
        if (self::getUser($transcriptId)) {
            Warning::set('Użytkownik o podanym numerze już istnieje');
            return FALSE;
        }
        $password = password_hash($password, PASSWORD_BCRYPT, array("cost" => 12));
        $user = new User((int)$transcriptId, (string)$password);
        $query = 'INSERT INTO `users`(`transcriptId`, `password`) VALUES (:transcriptId, :password)';
        return (Warning::count() === $errors && Db::executeQuery($query, $user)) ? $user : FALSE;
    }

    /** Pobiera dane użytkownika o podanym numerze indeksu z bazy danych i zwraca obiekt User
     * @param $transcriptId
     * @return bool|null
     */
    static public function getUser($transcriptId)
    {
        $errors = Warning::count();
        if (!isset($user) || empty($user)) {
            $user = (object)['transcriptId' => (int)$transcriptId];
            $query = 'SELECT * FROM users WHERE transcriptId = :transcriptId';
            $result = Db::executeQuery($query, $user);
            $user = ($result) ? $result->fetchObject() : NULL;
        }
        return ($errors === Warning::count() && !empty($user)) ? $user : FALSE;
    }

    static public function getUserType($transcriptId = null)
    {
        if (!empty($transcriptId)) {
            if ($transcriptId < 10) {
                return 'Admin';
            } else if ($transcriptId < 1000) {
                return 'Wykładowca';
            } else {
                return 'Student';
            }
        } else return 'Niezalogowany';

    }

    /** Usuwa uzytkownika o podanym numerze indeksu
     * @param $transcriptId
     * @return bool
     */
    static public function deleteUser($transcriptId)
    {
        $query = 'DELETE FROM users WHERE transcriptId=:transcriptId';
        $remove = Db::executeQuery($query, User::getUser($transcriptId));
        if ($remove) {
            Warning::set('Użytkownik usunięty');
            return FALSE;
        } else {
            Warning::set('Błąd podczas usuwania użytkownika');
            return FALSE;
        }
    }

    /**
     * @param null $password
     * @return bool
     */
    public function setPassword($password): bool
    {
        if (Validate::password($password)) {
            $this->password = $password;
            return TRUE;
        } else return FALSE;
    }
}
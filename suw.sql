-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 26 Lis 2018, 00:09
-- Wersja serwera: 5.7.23
-- Wersja PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `suw`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `courses`
--

CREATE TABLE `courses`
(
  `courseId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`courseId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `courses`
--

INSERT INTO `courses` (`courseId`, `name`, `userId`) VALUES
(1, 'PAIP', 2),
(2, 'PSIAI', 3),
(3, 'Podstawy teleinformatyki', 3),
(4, 'Jakis Kurs', 2),
(5, 'Drugi kurs', 2),
(6, 'Podstawy podstaw', 3),
(7, 'Podstawy Zaawansowania', 3),
(8, 'Zaawansowany Kurs Nic-nie-robienia', 3),
(9, 'Kurs Kursowania', 2),
(10, 'Zaawansowany Kurs', 2),
(11, 'Kurs kursora', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `downloads`
--

CREATE TABLE `downloads`
(
  `downloadId` int(11) NOT NULL AUTO_INCREMENT,
  `downloadTime` datetime NOT NULL,
  `fileId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`downloadId`),
  KEY `userId` (`userId`),
  KEY `fileId` (`fileId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='fixed';

--
-- Zrzut danych tabeli `downloads`
--

INSERT INTO `downloads` (`downloadId`, `downloadTime`, `fileId`, `userId`, `ip`) VALUES
(1, '2018-11-17 15:42:03', 2, 7, '127.0.0.1'),
(2, '2018-11-18 16:28:36', 3, 7, '192.168.1.3'),
(3, '2018-10-18 16:30:32', 4, 5, '192.168.1.3'),
(4, '2018-11-19 18:51:15', 2, 7, '127.0.0.1'),
(5, '2018-11-21 02:38:52', 5, 6, '192.168.1.3'),
(6, '2018-11-21 02:38:59', 2, 7, '127.0.0.1'),
(7, '2018-11-21 02:39:02', 4, 5, '192.168.1.3'),
(8, '2018-11-21 02:39:20', 5, 6, '127.0.0.1'),
(9, '2018-11-21 02:39:22', 3, 5, '192.168.1.3'),
(10, '2018-11-21 02:39:22', 2, 7, '192.168.1.3'),
(11, '2018-11-21 02:39:22', 1, 5, '192.168.1.3'),
(12, '2018-11-21 03:39:22', 5, 6, '192.168.1.3'),
(13, '2018-11-21 02:39:22', 3, 7, '192.168.1.3'),
(14, '2018-11-21 06:39:22', 2, 5, '192.168.1.3'),
(15, '2018-11-21 02:39:22', 1, 7, '192.168.1.3'),
(16, '2018-11-21 04:39:22', 2, 6, '192.168.1.3'),
(17, '2018-11-21 08:39:22', 5, 6, '192.168.1.3'),
(18, '2018-11-21 02:39:22', 3, 7, '192.168.1.3'),
(19, '2018-11-21 12:39:22', 2, 5, '192.168.1.3'),
(20, '2018-11-21 05:39:22', 4, 7, '192.168.1.3'),
(21, '2018-11-21 12:39:22', 4, 6, '192.168.1.3');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `files`
--

CREATE TABLE `files`
(
  `fileId` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `size` float NOT NULL,
  `uploadTime` datetime NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fileName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`fileId`),
  KEY `courseId` (`courseId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `files`
--

INSERT INTO `files` (`fileId`, `courseId`, `userId`, `title`, `description`, `size`, `uploadTime`, `type`, `fileName`) VALUES
(1, 4, 5, 'PDF 1', 'Jakis opis', 123, '2018-11-02 00:00:00', 'PDF', 'plik.pdf'),
(2, 5, 6, 'Podstawy teleinfy wyklady', 'Jakis opis tam', 432, '2018-11-04 00:00:00', 'PDF', 'PT.pdf'),
(3, 4, 6, 'Jakis tytul', 'Opis', 342, '2018-11-06 00:00:00', 'PDF', 'sadasd'),
(4, 4, 6, 'Plik jakiś', 'Opis', 342, '2018-11-06 00:00:00', 'PDF', 'PT.pdf'),
(5, 9, 7, 'Drugi plik', 'Opis', 342, '2018-11-06 00:00:00', 'PDF', 'plik.pdf');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `logged`
--

CREATE TABLE `logged`
(
  `userId` int(11) DEFAULT NULL,
  `sessionId` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `lastActivity` datetime DEFAULT NULL,
  PRIMARY KEY (`sessionId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
-- ----------------------------------------------------

--
-- Struktura tabeli dla tabeli `priviledges`
--

CREATE TABLE `priviledges`
(
  `priviledgeId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `fileId` int(11) NOT NULL,
  `clearanceLevel` tinyint(1) NOT NULL,
  PRIMARY KEY (`priviledgeId`),
  KEY `userId` (`userId`),
  KEY `fileId` (`fileId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `priviledges`
--

INSERT INTO `priviledges` (`priviledgeId`, `userId`, `fileId`, `clearanceLevel`) VALUES
(1, 6, 1, 0),
(2, 7, 4, 0),
(3, 6, 4, 0),
(4, 6, 3, 0),
(5, 5, 2, 0),
(6, 7, 5, 0),
(7, 2, 2, 1),
(8, 3, 3, 1),
(9, 3, 4, 1),
(10, 2, 1, 1),
(11, 2, 4, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `semester`
--

CREATE TABLE `semester`
(
  `current` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='1 - winter, 2 - summer';

--
-- Zrzut danych tabeli `semester`
--

INSERT INTO `semester` (`current`) VALUES
(1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users`
(
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `transcriptId` mediumint(9) NOT NULL,
  `password` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `lastActivity` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`userId`, `transcriptId`, `password`, `lastActivity`) VALUES
(1, 1, '$2y$12$padDy436UIu4dY/tCW/38.f/.qzzrdUxyGy5DcF.Ti8.V4zU38qTy', '2018-11-25 23:36:05'),
(2, 122, '$2y$12$padDy436UIu4dY/tCW/38.f/.qzzrdUxyGy5DcF.Ti8.V4zU38qTy', '2018-11-25 21:23:44'),
(3, 120, '$2y$12$padDy436UIu4dY/tCW/38.f/.qzzrdUxyGy5DcF.Ti8.V4zU38qTy', '2018-11-25 21:23:44'),
(4, 55555, '$2y$12$padDy436UIu4dY/tCW/38.f/.qzzrdUxyGy5DcF.Ti8.V4zU38qTy', '2018-11-25 21:23:44'),
(5, 44444, '$2y$12$padDy436UIu4dY/tCW/38.f/.qzzrdUxyGy5DcF.Ti8.V4zU38qTy', '2018-11-25 21:23:44'),
(6, 12345, '$2y$12$padDy436UIu4dY/tCW/38.f/.qzzrdUxyGy5DcF.Ti8.V4zU38qTy', '2018-11-26 00:03:26'),
(7, 12344, '$2y$12$padDy436UIu4dY/tCW/38.f/.qzzrdUxyGy5DcF.Ti8.V4zU38qTy', '2018-11-25 23:24:25');

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `downloads`
--
ALTER TABLE `downloads`
  ADD CONSTRAINT `downloads_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `downloads_ibfk_2` FOREIGN KEY (`fileId`) REFERENCES `files` (`fileId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_ibfk_1` FOREIGN KEY (`courseId`) REFERENCES `courses` (`courseId`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `files_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `logged`
--
ALTER TABLE `logged`
  ADD CONSTRAINT `logged_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `priviledges`
--
ALTER TABLE `priviledges`
  ADD CONSTRAINT `priviledges_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `priviledges_ibfk_2` FOREIGN KEY (`fileId`) REFERENCES `files` (`fileId`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

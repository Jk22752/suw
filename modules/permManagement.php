<?php
Load::security();


function get_courses(){
    $query = "SELECT courses.courseId, courses.name FROM courses";
    $result=db::executeQuery($query);
    return $result->fetchAll(db::FETCH_ASSOC);
}


function get_files_for_course($selected_course){
    $courses = get_courses();
    $selected_course_id = "";
    foreach($courses as $course) {
        if ($course['name'] == $selected_course){
            $selected_course_id = $course['courseId'];
            break;
        }
    }
    $query = "SELECT files.fileId, files.title FROM files WHERE files.courseId=".$selected_course_id;
    $result=db::executeQuery($query);
    return $result->fetchAll(db::FETCH_ASSOC);
}


function get_users(){
    $query = "SELECT users.userId, users.transcriptId from users";
    $result=db::executeQuery($query);
    return $result->fetchAll(db::FETCH_ASSOC);
}


function modify_permissions($selected_course, $perm_add_take, $selected_user, $selected_file) {

    $filtered_users = array();
    $users = get_users();
    if ($selected_user !== 'Wszyscy') {
        foreach($users as $user){
            if ($user['transcriptId'] == $selected_user){
                array_push($filtered_users, $user);
            }
        }
    } else {
        $filtered_users = $users;
    }

    $filtered_files = array();
    $files = get_files_for_course($selected_course);
    if ($selected_file !== 'Wszystkie') {
        foreach($files as $file){
            if ($file['title'] === $selected_file) {
                array_push($filtered_files, $file);
            }
        }
    } else {
        $filtered_files = $files;
    }

    if ($perm_add_take == 'perm_add'){
        $clearance_level = 1;
    } else {
        $clearance_level = 0;
    }
    foreach ($filtered_users as $user){
        foreach ($filtered_files as $file){
            $query = 'SELECT COUNT(1) FROM priviledges WHERE priviledges.userId='.$user['userId'].' AND priviledges.fileId='.$file['fileId'];
            $result=db::executeQuery($query);
            $counter = $result->fetchAll(db::FETCH_ASSOC);
            foreach ($counter as $c){
                if ($c['COUNT(1)'] == 0){
                    $query_ins = "INSERT INTO priviledges (priviledges.userId, priviledges.fileId, priviledges.clearanceLevel) VALUES (".$user['userId'].", ".$file['fileId'].", ".$clearance_level.")";
                    db::executeQuery($query_ins);
                } else {
                    $query_up = "UPDATE priviledges SET priviledges.clearanceLevel='.$clearance_level.' WHERE priviledges.userId=".$user['userId']." AND priviledges.fileId=".$file['fileId'];
                    db::executeQuery($query_up);
                }    
            }
        }
    }
}

?>

<body>
    <h3>Zarządzanie uprawnieniami</h3>
    <h5>Nadaj uprawnienia użytkownikom lub grupom.<h5>
<br>

<div class="form-group">
<form name="Courses" method="post" action="">
<p>
<label for="select_file">Kurs:</label>
    <select name='select_course' class="form-control">
        <?php
            $courses = get_courses();
            foreach($courses as $course) {
                print('<option value="'.$course['name'].'">'.$course['name'].'</option>');
            }
        ?>
    </select>
</p>
<p>
    <label>
        <input type="submit", name="submit_course" value="Wybierz kurs" class="btn btn-primary">
    </label>
</p>
</form>
</div>
<?php
    if (isset($_POST['submit_course'])) {
        $selected_course = $_POST['select_course'];
        $_SESSION['selected_course'] = $selected_course;
        print('Wybrano: '.$selected_course.'<br>');
        ?>
            <div class="form-group">
            <form name="Files_Users" method="post" action="">
                <p>
                    <label for="select_file">Plik:</label>
                    <select name="select_file" class="form-control">
                        <?php
                            $files = get_files_for_course($_SESSION['selected_course']);
                            print('<option value="Wszystkie">Wszystkie</option>');
                            foreach($files as $file){
                                print('<option value="'.$file['title'].'">'.$file['title'].'</option>');
                            }
                        ?>
                    </select>
                    <label for="select_user">Użytkownik: </label>
                    <select name="select_user" class="form-control">
                        <?php
                            $users = get_users();
                            print('<option value="Wszyscy">Wszyscy</option>');
                            foreach($users as $user){
                                print('<option value="'.$user['transcriptId'].'">'.$user['transcriptId'].'</option>');
                            }
                        ?>
                    </select>
                </p>
                <ul class="list-group">
                    <li class="list-group-item list-group-item-action"><input type="radio" name="radio_uprawnienia" value="perm_add" checked>Dodaj</li>
                    <li class="list-group-item list-group-item-action"><input type="radio" name="radio_uprawnienia" value="perm_take">Odbierz</li>
                    <li class="list-group-item list-group-item-action"><input type="submit", name="submit_all" value="DONE" class="btn btn-primary"></li>
                </ul>
            </form>
            </div>
        <?php
    }
    if (isset($_POST['submit_all'])) {
        $perm_add_take = $_POST['radio_uprawnienia'];
        $selected_user = $_POST['select_user'];
        $selected_file = $_POST['select_file'];
        modify_permissions($_SESSION['selected_course'], $perm_add_take, $selected_user, $selected_file);
        echo "<h4>Zmodyfikowano uprawnienia dla kursu: ".$_SESSION['selected_course']."</h4>"; 
    }
?>
</body>

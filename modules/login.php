<?php
Load::security();
if (isset($_GET['action']) && !empty($_GET['action'])) {
    $action = $_GET['action'];
    if ($action === 'logout') {
        Login::logOut('Wylogowano pomyślnie');
    }
} else if (Login::$logged) {
    ?>
    <div class="login text-center">
        <h5>Jesteś już zalogowany jako <?= Login::$login ?></h5>
        <h5>Jeżeli to nie Ty, to proszę się <a href="?page=login&action=logout"> wylogować.</a></h5>
    </div>
    <?php
    return TRUE;
} else if (isset($_POST['login'])) {
    $transcriptId = Validate::transcriptId($_POST['transcriptId']) ? $_POST['transcriptId'] : NULL;
    $password = !empty($_POST['password']) ? $_POST['password'] : NULL;
    if (!empty($transcriptId) && !empty($password)) {
        $user = User::getUser($transcriptId);
        if ($user && password_verify($password, $user->password)) {
            if (Login::addLogged($user)) {
                http_response_code(205);
                header('Location: index.php');
                exit();
            }
        }
    }
    Warning::set('Wpisany numer użytkownika lub hasło jest nieprawidłowe');
}
?>
<div class="login text-center">
    <h5>Aby uzyskać dostęp do pełnej funkcjonalności musisz się zalogować.</h5>
    <form action="?page=login" method="post">
        <div class="form-group">
            <label>Wprowadź swoje dane do logowania:</label>
            <input type="text" class="form-control" name="transcriptId" id="inputLogin"
                   placeholder="Twój numer użytkownika" autofocus>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Twoje hasło">
        </div>
        <button type="submit" name="login" value="Zaloguj" class="btn btn-primary" style="width:150px">Zaloguj</button>
    </form>
</div>
<?php
Load::security();
$userPanelActions = array('passwordChange');
?>
<div>
    <h2>Użytkownik</h2>
    <?php
    echo Login::$login . " - ";
    echo User::getUserType(Login::$login);
    ?>
    <hr/>
    <div>
        <h5>
            <a href="?page=userPanel&action=passwordChange">Zmiana hasła</a>
        </h5> <!-- więcej opcji w submenu? -->
    </div>
    <div>
        <?php
        if (!isset($_GET['action']) || !in_array($_GET['action'], $userPanelActions) || empty($_GET['action'])) {
            echo "Witaj w panelu zarządzania kontem. Wybierz akcję z submenu powyżej.";
        } else {
            if ($_GET['action'] == "passwordChange") {
                if (!isset($_POST['passwordChangeSubmit']) || empty($_POST['passwordChangeSubmit'])) {
                    echo "
                        <form name=\"passwordChangeForm\" method=\"post\">
                        <table>
                            <tr><td>Obecne hasło:</td><td><input type=\"password\" name=\"passwordChangeOldPassword\" /></td></tr>
                            <tr><td>Nowe hasło:</td><td><input type=\"password\" name=\"passwordChangeNewPassword\" /></td></tr>
                            <tr><td>Powtórz nowe hasło:</td><td><input type=\"password\" name=\"passwordChangeNewPasswordRepeat\" /></td></tr>
                        </table>
                            <input class=\"btn btn-primary\" type=\"submit\" name=\"passwordChangeSubmit\" value=\"Zmień\" />
                        </form>
                        ";
                } else {
                    $passwordQuery = "SELECT password FROM users WHERE users.transcriptId = ".Login::$login;
                    $res = Db::executeQuery($passwordQuery);

                    if ($res) {
                        $oldPassword = Validate::cleanInput($_POST['passwordChangeOldPassword']);
                        $passwordHashFromDatabase = $res->fetchColumn();

                        if (password_verify($oldPassword, $passwordHashFromDatabase)) {
                            $newPassword = Validate::cleanInput($_POST['passwordChangeNewPassword']);
                            $newPasswordRepeat = Validate::cleanInput($_POST['passwordChangeNewPasswordRepeat']);

                            if (Validate::password($newPassword)) {
                                if ($newPassword === $oldPassword) {
                                    Warning::set('Nowe hasło nie może być takie jak stare! Wypełnij formularz ponownie.');
                                    Warning::print();
                                } else {
                                    if ($newPassword === $newPasswordRepeat) {
                                        $newPasswordHashed = password_hash($newPassword, PASSWORD_BCRYPT, array("cost" => 12)); //hashowanie identyczne jak w User
                                        $passwordUpdateQuery = "UPDATE users SET password = '$newPasswordHashed' WHERE users.transcriptId = ".Login::$login;

                                        $res = Db::executeQuery($passwordUpdateQuery);

                                        if ($res) {
                                            echo "Zmiana hasła przebiegła pomyślnie!";
                                        } else {
                                            Warning::set('Wystąpił błąd');
                                            Warning::print();
                                        }
                                    } else {
                                        Warning::set('Nowe hasło nie zgadza się! Wypełnij formularz ponownie.');
                                        Warning::print();
                                    }
                                }
                            } else {
                                Warning::print();
                            }
                        } else {
                            Warning::set('Obecne hasło niepoprawne! Wypełnij formularz ponownie.');
                            Warning::print();
                        }
                    } else {
                        Warning::set('Wystąpił błąd');
                        Warning::print();
                    }
                }
            }
        }
        ?>
    </div>
</div>
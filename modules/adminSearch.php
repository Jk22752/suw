<?php
Load::security();

$warnings=Warning::count();
if (isset($_GET['search'])) {
    if (isset($_GET['mod']) && !empty($_GET['mod']) && in_array($_GET['mod'], array_keys(Admin::$tables))) {
        $module = $_GET['mod'];
        if (isset($_GET['field']) && !empty($_GET['field']) && $field=array_search($_GET['field'], Admin::$tables[$module])) {
            if (isset($_GET['value']) && strlen($_GET['value'])>0) {
                $value = Validate::cleanInput($_GET['value']);
                $searchObject = (object)[$field => "$value"];
                if (isset($_GET['accurate']) && !empty($_GET['accurate'])&&(bool)$_GET['accurate']) {
                    $result = Db::executeQuery("SELECT * FROM $module WHERE $field=:$field",$searchObject);
                } else $result = Db::executeQuery("SELECT * FROM $module WHERE $field LIKE '%$value%'");
                $result=$result->fetchAll(Db::FETCH_ASSOC);
                reset($result[0]);
                $idName = key($result[0]);
                if (!empty($result) && count($result) > 1) {
                    Table::create($result);
                } else if (count($result) === 1) {
                    ob_end_clean();
                    header("Location: ?page=adminShow&mod=$module&id=" . $result[0][$idName]);
                    exit();
                } else Warning::set('Brak wyników wyszukiwania');
            } else Warning::set('Wpisz co chcesz wyszukać');
        } else Warning::set('Błędne pole do wyszukania');
    } else Warning::set('Błędny moduł do wyszukania');
}
if ($warnings!==Warning::count()) load::getPage('admin');
echo "<a href=\"index.php?page=admin&mod=$module\"><h4>Powrót do wyboru modułów</h4></a>";
?>

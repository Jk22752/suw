<?php
Load::security();
if (isset($_GET['mod']) && !empty($_GET['mod']) && array_key_exists($_GET['mod'], Admin::$tables)) {
    $module = $_GET['mod'];
    if (isset($_GET['id']) && Validate::number($_GET['id'])) {
        $id = $_GET['id'];
        $idName = array_flip(Admin::$tables[$module]);
        reset($idName);
        $idName = $idName[key($idName)];
        $edit = FALSE;
        $fileChange = false;
        if (isset($_POST['change'])) {
            unset($_POST['change']);
            if (isset($_POST['fileNameOld'])) {
                $fileNameOld = $_POST['fileNameOld'];
                unset($_POST['fileNameOld']);
                $fileChange = true;
            }
            $edit = TRUE;
            $data = $_POST;
            $errors = Warning::count();
            $data = Admin::validateData($data);
            if ($errors === Warning::count()) {
                if ($fileChange) {
                    $filePath = Config::get('projectDirAbsPath') . DIRECTORY_SEPARATOR . Config::get('defaultContentStorage') . DIRECTORY_SEPARATOR;
                    if (!file_exists($filePath . $data['fileName'])) {
                        if (file_exists($filePath . $fileNameOld)) {
                            rename($filePath . $fileNameOld, $filePath . $data['fileName']);
                        } else Warning::set('Plik nie istnieje');
                    } else Warning::set('Istnieje już plik o podanej nazwie');
                }
            }
            if ($errors === Warning::count()) {
                $insertObject = (object)$data;
                $values = '';
                foreach ($data as $key => $val) {
                    $values .= $key . '=:' . $key . ',';
                }
                $values = rtrim($values, ',');
                $insert = Db::executeQuery("UPDATE $module SET $values WHERE $idName=$id", $insertObject);
                if ($insert) {
                    Warning::set('Zmiana danych zakończona pomyślnie');
                }
            }
            $result = $data;
        }
        $searchObject = (object)[$idName => $id];
        if (empty($result)) {
            $result = Db::executeQuery("SELECT * FROM $module WHERE $idName=:$idName", $searchObject)->fetchAll(Db::FETCH_ASSOC);
            $result = $result[0];
        }
        if (!empty($result)) {
            ?>
            <form action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
                <?php
                $i = 0;
                foreach ($result as $field => $value) {
                    if ($i++ === 0 && !$edit) {
                        continue;
                    }
                    echo '<div class="form-group"><label for="' . $field . '">' . Admin::$tables[$module][$field] . '</label>: <input class="form-control" name="' . $field . '" type="text" value="' . $value . '"></div><br />';
                }
                if (array_key_exists('fileName', $result)) {
                    $fileNameOld = $result['fileName'];
                    echo '<input type="hidden" name="fileNameOld" value="' . $fileNameOld . '">';
                }
                ?>
                <button type="submit" name="change" class="btn btn-primary">Zmień</button><a href="index.php?page=adminDelete&mod=<?= $module ?>&idName=<?= $idName ?>&id=<?= $id ?>" class="btn btn-danger">Usuń</a>
            </form>
            <?php
        }
    }
    echo "<a href=\"index.php?page=admin&mod=$module\"><h4>Powrót do wyszukiwania</h4></a>";
} else
    echo "<a href=\"index.php?page=admin\"><h4>Powrót</h4></a>";
?>
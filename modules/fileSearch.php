<?php
Load::security();
?>
    <h3>Materiały</h3>
<?php
# Przeglądanie materiałów

if (!isset($_POST['submitCourseSelect']) || empty($_POST['submitCourseSelect'])) {
    $coursesQuery = "SELECT DISTINCT courses.courseId, courses.name FROM courses 
                        JOIN files ON courses.courseId=files.courseId 
                        JOIN priviledges ON files.fileId=priviledges.fileId 
                        JOIN users ON priviledges.userId=users.userId 
                        WHERE users.transcriptId = ".Login::$login;
    $res = Db::executeQuery($coursesQuery);

    if ($res) {
        $rows = $res->fetchAll();

        echo "Przeglądaj dostępne wykłady/kursy: <br />";
        echo "<form name=\"courseSelect\" method=\"post\"><select name=\"selectedCourse\">";
        foreach ($rows as $course) {
            echo "<option value=\"" . $course['courseId'] . "\">" . $course['name'] . "</option>";
        }
        echo "</select><br />";

        echo "<input class=\"btn btn-primary\" type=\"submit\" name=\"submitCourseSelect\" value=\"Przejdź\" /></form>";
    } else {
        Warning::set('Brak kursu/ów w bazie danych!');
    }
} else {
    if (!isset($_POST['fileName']) || empty($_POST['fileName'])) {
        $courseId = $_POST['selectedCourse'];

        $filesQuery = "SELECT DISTINCT files.fileId, files.fileName, files.title, courses.name
                                    FROM files
                                    JOIN priviledges ON files.fileId = priviledges.fileId
                                    JOIN users ON priviledges.userId = users.userId
                                    JOIN courses ON files.courseId = courses.courseId
                                    WHERE users.transcriptId = " . Login::$login . " AND courses.courseId = $courseId AND priviledges.clearanceLevel=0";
        $res = Db::executeQuery($filesQuery);
        if ($res) {
            $rows = $res->fetchAll();
            if (empty($rows[0]['name']) || !isset($rows[0]['name'])) {
                Warning::set('Brak udostępnionych materiałów w bazie danych');
                Warning::print();
            } else {
                print_r("Materiały dla kursu <b>" . $rows[0]['name'] . "</b>:<br /><br />");

                echo "Dostępne: <br /><br />";

                echo "<form name=\"courseSelect\" method=\"post\" target=\"_blank\">";
                echo "<input type=\"hidden\" name=\"submitCourseSelect\" value=\"Przejdź\" />";
                foreach ($rows as $key => $fileRow) {
                    echo "<input type=\"radio\" name=\"fileName\" value=\"" . $fileRow['fileId'] . "\"" . (($key === 0) ? ' checked' : '') . ">" . $fileRow['title'] . "<br />";
                }
                $password = uniqid(); 
                echo "<input type=\"hidden\" name=\"filePassword\" value=\"$password\" />";
                echo "<br />Hasło do pliku (generowane <b>TYLKO RAZ</b> - prosimy o zapisanie!): <b>$password</b> <br />";
                echo "<input class=\"btn btn-primary\" type=\"submit\" name=\"fileOpenSelect\" value=\"Otwórz\" />";
                echo "<input class=\"btn btn-primary\" type=\"submit\" name=\"fileDownloadSelect\" value=\"Pobierz\" />";
                echo "</form><br /><br />";
            }

        } else {
            Warning::set('Brak udostępnionych materiałów w bazie danych');
        }

        echo "<br /><a href='?page=fileSearch'>Powrót do listy Twoich kursów</a>.<br />";
    } else {
        $password = $_POST['filePassword'];
        if (isset($_POST['fileOpenSelect'])) {
            
            header("Location: ?page=fileDownload&fileId=" . $_POST['fileName'] . "&pwd=" . $password);
        } else {
            header("Location: ?page=fileDownload&fileId=" . $_POST['fileName'] . "&download=1" . "&pwd=" . $password);
            
        }
    }
}

# Historia pobrań

$downloadsQuery = "SELECT downloads.downloadTime, downloads.ip, files.title, files.fileName
                                    FROM downloads 
                                    JOIN users ON downloads.userId = users.userId 
                                    JOIN files ON files.fileId = downloads.fileId  
                                    WHERE users.transcriptId = " . Login::$login . " ORDER BY downloads.downloadTime DESC";
$res = Db::executeQuery($downloadsQuery);

if ($res) {
    $rows = $res->fetchAll(Db::FETCH_ASSOC);
    if (!$rows || count($rows) < 1) {
        echo "<span>Brak historii Twoich pobrań</span><br />";
    } else {
        echo "<br /><br />Twoja historia pobrań wykładów: <br /><table class=\"table table-striped\">";
        Table::create($rows, ['downloadTime' => 'Czas pobrania', 'ip' => 'IP użytkownika', 'title' => 'Tytuł pliku', 'fileName' => 'Nazwa pliku']);
    }
} else {
    echo "<span>Brak historii pobrań</span><br />";
}

?>		
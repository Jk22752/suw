<?php
Load::security();
if (isset($_GET['fileId']) && !empty($_GET['fileId'])) {
    $fileId = $_GET['fileId'];
    $pwd = $_GET['pwd'];
    $fileService = new FileService(Config::get('defaultContentStorage'));
    $filePath = $fileService->getFilePath($fileId);

    $priviledgesQuery = "SELECT priviledges.userId, priviledges.fileId
                        FROM priviledges
                        JOIN users ON users.userId=priviledges.userId 
                        WHERE priviledges.fileId = $fileId AND users.transcriptId = ".Login::$login;
    $res = Db::executeQuery($priviledgesQuery);


    if($res)
    {
        $rows = $res->fetchAll();
        if (empty($rows[0]) || !isset($rows[0])) {
            Warning::set('Brak udostępnionych materiałów w bazie danych');
            Warning::print();
        } else if (!empty($filePath) && file_exists($filePath)) {
            $waterm = new AddWatermark($fileId);
            $date = date('Y-m-d H:i:s');
            $text = array(Login::$login, $date, $_SERVER['REMOTE_ADDR']);
            $waterm->setText($text);
            $waterm->pdf_password = $pwd;
            $file = $waterm->add_watermark();
            ob_end_clean();
            if (isset($_GET['download'])) {
                $waterm->downloadFile(basename($filePath));
            } else {
                $waterm->outputFile(basename($filePath));
            }
        } else {
            Warning::set('Brak pliku w bazie danych.');
            Warning::print();
        }
    }
} else {
    header('Location: index.php');
}
?>
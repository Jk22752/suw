<?php
Load::security();
$errors=Warning::count();
$id=(isset($_GET['id'])&&Validate::number($_GET['id'])?$_GET['id']:Warning::set('Nieprawidłowe id'));
$mod=(isset($_GET['mod'])&&array_key_exists($_GET['mod'], Admin::$tables)?$_GET['mod']:Warning::set('Nieprawidłowy moduł'));
$idField=(isset($_GET['idName'])&&array_key_exists($_GET['idName'], Admin::$tables[$mod])?$_GET['idName']:Warning::set('Nieprawidłowa nazwa pola'));
if ($errors===Warning::count()) {
    if (isset($_GET['confirm']) && $_GET['confirm']) {
        $deleteObject = (object)[$idField => $id];
        if ($idField == 'fileId') {
            $fileNameQuery = "SELECT fileName FROM files WHERE fileId = $id";
            $fileName = Db::executeQuery($fileNameQuery)->fetchColumn();
            $filePath = Config::get('projectDirAbsPath') . DIRECTORY_SEPARATOR . Config::get('defaultContentStorage') . DIRECTORY_SEPARATOR;
            if (file_exists($filePath . $fileName)) {
                unlink($filePath . $fileName);
                Warning::set('Plik usunięty pomyślnie');
            } else Warning::set('Plik nie istnieje.');
        }
        $query = "DELETE FROM $mod WHERE $idField=:$idField";
        $result = Db::executeQuery($query, $deleteObject);
        if ($result) {
            Warning::set('Rekord usunięty pomyślnie');
            ob_end_clean();
            header("Location: index.php?page=admin&mod=" . $mod);
            exit();
        } else Warning::set('Błąd podczas usuwania rekordu');

    }
    ?>    <h1>Czy na pewno chcesz usunąć rekord z tabeli "<?= $mod ?>" o id równym "<?= $id ?>"?</h1>
    <a href="index.php?page=adminDelete&mod=<?= $mod ?>&idName=<?= $idField ?>&id=<?= $id ?>&confirm=true"
       class="btn btn-danger">Potwierdzam</a>
    <?php
}
?>
 <a href="index.php?page=adminShow&mod=<?= $mod ?>&id=<?= $id ?>" class="btn btn-primary">Powrót</a>

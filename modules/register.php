<?php
Load::security();
if (isset($_POST['register'])) {
    $errors = Warning::count();
    $transcriptId = !empty($_POST['transcriptId']) && Validate::transcriptId($_POST['transcriptId']) ? Validate::cleanInput($_POST['transcriptId']) : NULL;
    $password = !empty($_POST['password']) && Validate::password($_POST['password']) ? Validate::cleanInput($_POST['password']) : NULL;
    $password_repeat = !empty($_POST['password_repeat']) ? Validate::cleanInput($_POST['password_repeat']) : NULL;
    if ($password !== $password_repeat) {
        Warning::set('Hasła nie są takie same');
    } else if ($errors === Warning::count()) {
        if (User::setUser($transcriptId, $password)) {
            Warning::set('Rejestracja przebiegła pomyślnie');
            ob_end_clean();
            header('Location: index.php');
            exit();
        }
    }
}
?><h1>Rejestracja użytkownika</h1>
<?php
if (!isset($_GET['userType']) || empty($_GET['userType'])) {
    ?>
    <p><a href="index.php?page=register&userType=student"><h4>Zarejestruj się jako student</h4></a></p>
    <p><a href="index.php?page=register&userType=lecturer"><h4>Zarejestruj się jako wykładowca</h4></a></p>
    <?php
} else {
    $userType = Validate::cleanInput($_GET['userType']);
    ?>
    <form action="index.php?page=register&userType=<?= $_GET['userType'] ?>" method="post">
        <div class="form-group">
            <label for="transcriptId">Numer użytkownika</label>
            <?php if ($userType === 'student') { ?>
            <input type="text" id="transcriptId" name="transcriptId" placeholder="Twój numer indeksu"
                   value="<?= isset($transcriptId) && !empty($transcriptId) ? $transcriptId : '' ?>"
                   class="form-control" autofocus>
        </div>
        <?php } else {
            $transcriptId = Db::executeQuery('SELECT MAX(transcriptId) FROM users WHERE transcriptId BETWEEN 99 AND 999');
            $transcriptId = $transcriptId->fetch(Db::FETCH_NUM);
            $transcriptId[0]++;
            ?>
            <strong><?= $transcriptId[0] ?> - ZANOTUJ!</strong>
            <input type="hidden" id="transcriptId" name="transcriptId" value=<?= $transcriptId[0] ?>>
            </div>
        <?php } ?>
        <div class="form-group"><label for="password">Hasło</label>
            <input type="password" id="password" name="password" placeholder="Twoje hasło" class="form-control">
        </div>
        <div class="form-group">
            <input type="password" id="password_repeat" name="password_repeat" placeholder="Powtórz hasło"
                   class="form-control">
        </div>
        <input type="submit" name="register" value="Zajerestruj się" class="btn btn-primary">
    </form>
    <a href="index.php?page=register">Powrót</a>
<?php }
?>
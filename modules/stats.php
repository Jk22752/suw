<?php
Load::security();

$usersCount = "XXX"; //total registered users count
$dailyDL = "xxx"; //total dl from present day
$monthlyDL = "xxx"; //total dl from current month
$semestrallyDL = "xxx"; //total dl from current semester

$todayBeginDate = date('Y-m-d 00:00:00');
$todayEndDate = date('Y-m-d 23:59:59');
$firstDayOfMonthDate = date('Y-m-01 00:00:00');
$lastDayOfMonthDate = date('Y-m-t 23:59:59');

$currentSemester = 1; //1 - winter, 2 - summer //Równie dobrze może być 0/1, whatever
$semesterQuery = "SELECT current FROM semester";
$res = Db::executeQuery($semesterQuery);
if ($res) {
    $semester = $res->fetchColumn();
    $currentSemester = $semester;
}
if ($currentSemester == 1) {
    $firstDayOfSemesterDate = date('Y-10-01 00:00:00');
    $lastDayOfSemesterDate = date('Y-02-24 23:59:59', strtotime('+1 year'));
} else {
    $firstDayOfSemesterDate = date('Y-02-25 00:00:00');
    $lastDayOfSemesterDate = date('Y-07-01 23:59:59');
}

$usersCountQuery = "SELECT transcriptId FROM users";
$res = Db::executeQuery($usersCountQuery);
if ($res) {
    $countArray = $res->fetchAll();
    $usersCount = count($countArray);
}

$dailyDLQuery = "SELECT COUNT(*) FROM downloads WHERE downloadTime BETWEEN '$todayBeginDate' AND '$todayEndDate'";
$res = Db::executeQuery($dailyDLQuery);
if ($res) {
    $count = $res->fetchColumn();
    $dailyDL = $count;
}

$monthlyDLQuery = "SELECT COUNT(*) FROM downloads WHERE downloadTime BETWEEN '$firstDayOfMonthDate' AND '$lastDayOfMonthDate'";
$res = Db::executeQuery($monthlyDLQuery);
if ($res) {
    $count = $res->fetchColumn();
    $monthlyDL = $count;
}

$semestrallyDLQuery = "SELECT COUNT(*) FROM downloads WHERE downloadTime BETWEEN '$firstDayOfSemesterDate' AND '$lastDayOfSemesterDate'";
$res = Db::executeQuery($semestrallyDLQuery);
if ($res) {
    $count = $res->fetchColumn();
    $semestrallyDL = $count;
}

//Oddzielenie UI od logiki
?>
<ul class="list-group">
    <li class="list-group-item list-group-item-secondary"><strong>Liczba użytkowników:</strong></li>
    <li class="list-group-item d-flex justify-content-between align-items-center">Ogółem: <span
                class="badge badge-primary badge-pill"><?php echo $usersCount; ?></span></li>
    <li class="list-group-item d-flex justify-content-between align-items-center">Zalogowanych: <span
                class="badge badge-primary badge-pill"><?= Login::getCurrentLogged(); ?></span></li>
    <li class="list-group-item list-group-item-secondary"><strong>Liczba pobrań:</strong></li>
    <li class="list-group-item d-flex justify-content-between align-items-center">dzień: <span
                class="badge badge-primary badge-pill"><?php echo $dailyDL; ?></span></li>
    <li class="list-group-item d-flex justify-content-between align-items-center">miesiąc: <span
                class="badge badge-primary badge-pill"><?php echo $monthlyDL; ?></span></li>
    <li class="list-group-item d-flex justify-content-between align-items-center">semestr: <span
                class="badge badge-primary badge-pill"><?php echo $semestrallyDL; ?></span></li>
</ul>

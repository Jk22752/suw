<?php
$mod = isset($_GET['mod']) && !empty($_GET['mod']) ? $_GET['mod'] : null;
if (!empty($mod) && in_array($mod, array_keys(Admin::$tables))) {
    ?>
    <h3>Moduł administratora:</h3>
    <div class="card">
    <div class="card-body">
        <h5 class="card-title">Wyszukiwarka</h5>
    <form class="form-inline" action="index.php" method="get">
        <input type="hidden" name="page" value="adminSearch">
        <input type="hidden" name="mod" value="<?= $mod ?>">
        <div class="form-row align-items-center">
            <div class="col-auto ">
                <input type="text" id="value" class="form-control form-control-sm mb-2 mr-sm-2" name="value" placeholder="Wpisz frazę do wyszukania">
            </div>
            <div class="col-auto">
        <select class="form-control form-control-sm mb-2 mr-sm-2" id="field" name="field" >
            <?php foreach (Admin::$tables[$mod] as $field) echo '<option>'.$field.'</option>'; ?>
        </select>
            </div>
            <div class="col-auto">
                <label class="form-check-label mr-sm-2">
                    <input class="form-check-input" name="accurate" value="true" type="checkbox"> Wyszukiwanie<br />dokładne
                </label>
            </div>
            <div class="col-auto">
        <button type="submit" name="search" class="btn btn-primary mb-2">Submit</button>
        </div>
        </div>
    </form>
    </div>
    </div>
    <?php
    $query = Admin::$queries[$mod];
    $headers = Admin::$tables[$mod];
    $data = Db::executeQuery($query)->fetchAll(Db::FETCH_ASSOC);
    echo '<h4>' . Admin::$translations[$mod] . '</h4>';
    Table::create($data, $headers);
    echo '<a href="index.php?page=admin"><h4>Powrót do wyboru modułów</h4></a>';
} else {
    ?>
    <a href="index.php?page=admin&mod=users"><h4>Zarządzanie użytkownikami</h4></a>
    <a href="index.php?page=admin&mod=files"><h4>Zarządzanie plikami</h4></a>
    <a href="index.php?page=admin&mod=courses"><h4>Zarządzanie kursami</h4></a>
    <a href="index.php?page=admin&mod=courses"><h4>Zarządzanie kursami</h4></a>
    <a href="index.php?page=admin&mod=priviledges"><h4>Zarządzanie uprawnieniami</h4></a>
    <a href="index.php?page=admin&mod=downloads"><h4>Wyświetlanie logów</h4></a>
    <a href="index.php?page=admin&mod=logged"><h4>Wyświetlanie zalogowanych</h4></a>
    <?php
}
?>